const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];
let timeSpentFrontend = (tasks.filter(category => category.category === "Frontend")).reduce(function (p1, p2) {


    return p2.timeSpent + p1;
}, 0);
let timeSpentBug = (tasks.filter(type => type.type === "bug")).reduce(function (p1, p2) {


    return p2.timeSpent + p1;
}, 0);
let reg = new RegExp("UI");
let tasksUI = (tasks.filter(title => reg.test(title.title))).reduce(function (p1) {


    return p1 + 1;
}, 0);


let tasksCount = tasks.reduce(function (p1, p2, p3, p4) {


    if (p2.category === "Frontend")
        return {Front: p1.Front + 1, End: p1.End};
    else if (p2.category === "Backend")
        return {Front: p1.Front, End: p1.End + 1};
    else return p1;

}, {Front: 0, End: 0});

let moreFourHour = (tasks.filter(time => time.timeSpent > 4)).map(obj => {
    return {title: obj.title, category: obj.category}
});
console.log(timeSpentFrontend, "timeSpentFrontend");
console.log(timeSpentBug, "timeSpentBug");
console.log(tasksUI, "tasksUI");
console.log(tasksCount, "taskCount");
console.log(moreFourHour, "moreFourHour");