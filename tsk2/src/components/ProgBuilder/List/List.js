import React, {Component} from "react";
import Item from "./Item/Item";
import "./List.css"

const List = props => {
    let fullList = props.list;
    return (
        <div className="List">
            {fullList.map(item => {
                return <Item name={item.name} price={item.price} key={item.id} remove={()=>props.remove(item.id)}></Item>
            })}
            <p className="totalSum">Total sum: {props.total}</p>
        </div>
    );
};
export default List;