import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ProgBuilder from "./components/ProgBuilder/ProgBuilder";

class App extends Component {
  render() {
    return (
      <ProgBuilder/>
    );
  }
}

export default App;
