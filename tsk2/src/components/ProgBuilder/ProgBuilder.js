import React, {Component} from "react";
import Wrapper from "../../hoc/Wrapper";
import List from "./List/List";
import Menu from "./Menu/Menu";
class ProgBuilder extends Component {
    state = {
        list: [],
        total: 0
    };
    updateTotalPrice = (array) => {
        return array.reduce((total, next) => {
            return total + next.price;
        }, 0);


    };
    addNewElement = () => {
        const first = document.getElementsByClassName("itemName")[0].value;
        const second = +document.getElementsByClassName("Cost")[0].value;
        if (!first || !second) {
            alert("Поля должны быть заполнены!!!");
            return;
        }
        document.getElementsByClassName("itemName")[0].value = "";
        document.getElementsByClassName("Cost")[0].value = "";
        const tmpList = [...this.state.list];

        tmpList.push({name: first, price: second, id: Date.now()});

        this.setState({list: tmpList, total: this.updateTotalPrice(tmpList)});


    };

    cutItem = (cut, list) => list.filter(item => item != cut);

    removeElement = id => {


        const index = this.state.list.findIndex(p => p.id === id);
        console.log(index);
        let tmpList = [...this.state.list];
        tmpList = tmpList.filter(item => item != tmpList[index]);
        this.setState({list: tmpList, total: this.updateTotalPrice(tmpList)});

    };


    render() {
        return (<Wrapper>
            <Menu event={this.addNewElement}></Menu>
            <List list={this.state.list}
                  total={this.state.total}
                  remove={this.removeElement}></List>
        </Wrapper>)
    }

}
export default ProgBuilder;