import React from 'react';
import './Item.css';
const Item=props=>{
    return (<p className="Item">{`${props.name}    ${props.price}KGS`}  <i onClick={props.remove} >X</i> </p>);
};
export default Item;